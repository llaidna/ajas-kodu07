import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/*
 *  Lauri bitbucket https://bitbucket.org/llaidna/ajas-kodu07
 * 
 *  Mõistatuste lahendaminea
 *  Koostada programm, mis lahendaks kahe arvu liitmise kohta käivaid
 *  sõnamõistatusi, milles igale tähele tuleb vastavusse seada erinev
 *  kümnendnumber ning ühegi arvu alguses ei saa olla null (sõnad
 *  koosnevad ladina tähestiku suurtähtedest ning ühegi sõna pikkus ei
 *  ületa 18 tähte).
 *  
 *  Näiteks mõistatuse SEND + MORE = MONEY lahendiks on
 *  S=9, E=5, N=6, D=7, M=1, O=0, R=8, Y=2 (9567 + 1085 = 10652)
 *  Sõnad saadakse käsurea parameetritena (kolm sõna). Programm peaks
 *  mittelahenduva mõistatuse korral seda ütlema ning lahenduvale
 *  mõistatusele leidma vähemalt ühe lahendi. Väljundi võite vabalt
 *  kujundada, samuti kasutatavad andmestruktuurid.
 *  
 *  Lahendus esitage ikka https://moodle.e-ope.ee/course/view.php?id=2454
 *  Õigeaegne ja piisav lahendus annab 10 punkti.
 *  
 *  http://www.tutorialspoint.com/java/java_hashmap_class.htm
 *  
 *  jupitame sissetulevad sõnad
 *  kui kolmandas sõnas on rohkem char kui esimeses või teises sõnas, on kolmanda esimene char 1 (foundOne = true)
 *  
 *  võtame kõik tähed, paneme neile numbrid 0...n
 *  siis hakkame viimasele proovima +1 suuremaid väärtusi 10x
 *  kui ei klapi
 *  siis suurendama eelviimast +1 ja proovime ikkagi viimasele väärtusi (neid mis pole eespool käigus hetkel)
 *  kui ei klapi
 *  siis suurendame eelviimast +1 ja proovime ikkagi viimasele väärtusi (neid mis pole eespool käigus hetkel)
 *  
 *  HashMap itereerimise kood kopeeritud veebist!!!
 *  Timeout kood kopteeritud veebist!!! https://stackoverflow.com/questions/2275443/how-to-timeout-a-thread
 *  Permutatsioonide tegemise kood kopeeritud from http://enos.itcollege.ee/~jpoial/algoritmid/Lipud.java
 */

public class Puzzle {

	static String[] myArgs = new String[3];
	static Map<String, Integer> hm = new LinkedHashMap<String, Integer>();
	static LinkedList<String> ok = new LinkedList<String>(); // list, kus sees on all tried väärtused
	static LinkedList<String> list = new LinkedList<String>(); // list, kus sees on käigus olevad tähed
	static int[][] listlist; //list.size()
	
	int suurus;
	int[] laud;
	
	public static void main(String[] args) {

		if (args.length < 3) {
			System.out.println("palun sisesta 3 sõna argumentidena, eraldades need tühikuga ja kasutades sõnades vaid SUURI LADINA TAHTI.");
		}

		for (int i = 0; i < args.length; i++) {
			myArgs[i] = args[i];
			System.out.println("sisse myArgs: " + myArgs[i]);
		}

		String erandone = Character.toString(myArgs[0].charAt(0)); // sõna 1 esitäht mis ei tohi olla 0
		String erandtwo = Character.toString(myArgs[1].charAt(0)); // sõna 2 esitäht mis ei tohi olla 0
		String erandthree = Character.toString(myArgs[2].charAt(0)); // sõna 3 esitäht mis ei tohi olla 0
		System.out.println("erandid ehk algustähed mis ei tohi 0 olla: " + erandone + " & " + erandtwo + " & " + erandthree);

		LinkedList<Integer> values = new LinkedList<Integer>(); // list, kus sees on hetkel väärtustatud väärtused

		for (int i = 0; i < 3; i++) { // iterates number of words
			for (int j = 0; j < myArgs[i].length(); j++) { // iterates number of chars
				hm.put(Character.toString(myArgs[i].charAt(j)), null); // puts chars into hm
			}
		}

		// teeb ühekordse listi tähtedest - Map interaatori kood kopeeritud veebist!
		// Get a set of the entries
		Set<Entry<String, Integer>> set = hm.entrySet();
		// Get an iterator
		Iterator<Entry<String, Integer>> it = set.iterator();
		// Display elements
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry me = (Map.Entry) it.next();
			System.out.print(me.getKey() + "=");
			System.out.print(me.getValue());
			System.out.print(" ");
			list.add((String) me.getKey());
		}
		System.out.println();
		System.out.println("list = " + list);
		System.out.println("tähti on: " + list.size());

		// lipud
		System.out.println("Permutatsioonide tekitamine...");
		Lipud seis = new Puzzle().new Lipud(10); // laua suurus list.size()
		seis.paiguta();
		
		// paneme väärtused hm'i
		for (int i = 0; i < list.size(); i++) { // nii mitu tähte on kokku käigus
			hm.replace(list.get(i), i); // 1...9 elementidele. Võtab listist elemendi, näiteks "A" ja paneb hm'is sellele väärtuse
			values.add(i); // adds values to values list, which holds all the values currently in the list
		}
		System.out.println("hm is now: " + hm);

		// põhi kood!
		int done = 0;
		
		// while (arvuta() == false) {
		while (done < listlist.length && (listlist[done][0] != listlist[done][1])) {

			for (int i = 0; i < list.size(); i++) {
				hm.replace(list.get(i), listlist[done][i]);
			}

			done = done + 1;

			if ((hm.get(erandone) != 0) && (hm.get(erandtwo) != 0) && (hm.get(erandthree) != 0)) {
				arvuta();
			}

		}
		if (ok.size() == 0) {
			System.out.println("Ühtegi lahendit ei leidu! The End.");
		} else {
			System.out.println("Kõik " + ok.size() + " lahendit leitud! The End.");
		}
	}

	/**
	 * Arvutab
	 * 
	 * @return boolean
	 */
	public static boolean arvuta() {
		
		StringBuilder sbone = new StringBuilder();
		StringBuilder sbtwo = new StringBuilder();
		StringBuilder sbthree = new StringBuilder();
		long one = 0;
		long two = 0;
		long three = 0;

		for (int i = 0; i < myArgs[0].length(); i++) {
			sbone.append(hm.get(Character.toString(myArgs[0].charAt(i))));
		}
		for (int i = 0; i < myArgs[1].length(); i++) {
			sbtwo.append(hm.get(Character.toString(myArgs[1].charAt(i))));
		}
		for (int i = 0; i < myArgs[2].length(); i++) {
			sbthree.append(hm.get(Character.toString(myArgs[2].charAt(i))));
		}

		one = Long.parseLong(sbone.toString());
		two = Long.parseLong(sbtwo.toString());
		three = Long.parseLong(sbthree.toString());
		long sum = one + two;
		
		if (one + two == three) {
			// kontrollib, ega ei ole korduv lahend. Kui on, siis
			for (int i = 0; i < ok.size(); i++) {
				if (ok.get(i).equals(sbone.toString() + sbtwo.toString() + sbthree.toString())) {
					return false;
				}
			}

			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Leidsin lahendi !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.out.println("   " + one);
			System.out.println("   " + two);
			System.out.println("  " + sum);
			System.out.println("Summa on: " + sum + " ja peakski olema " + three);
			System.out.println("Õiged väärtused on =   " + hm);
			System.out.println();
			ok.add(sbone.toString() + sbtwo.toString() + sbthree.toString()); // lisab leitud lahendite nimistusse
			return true;
		}
		return false;
	}

	public class Lipud {

		/**
		 * laua algseis
		 * 
		 * @return
		 * @author jpoial
		 */
		Lipud(int n) {
			if (n < 1)
				throw new RuntimeException("valed mõõtmed");
			suurus = n;
			laud = new int[suurus];
			for (int i = 0; i < suurus; i++) {
				laud[i] = 0;
			}
		}

		/**
		 * laua seisu analüüs lipu nr. k jaoks: 1 <= k <= suurus-1 )
		 * 
		 * @author jpoial
		 * */
		public boolean onTules(int k) {
			if (k >= suurus)
				throw new RuntimeException("Vale lipu number");
			if (k < 0)
				return false;
			for (int i = 0; i < k; i++) {
				if (laud[i] == laud[k])
					return true; // samal real
				// if (k - i == Math.abs(laud[k] - laud[i]))
				// return true; // samal diagonaalil
			}
			return false;
		}

		/**
		 * paigutamine
		 * 
		 * @author jpoial
		 * @author lauri
		 */
		public void paiguta() {
			int n = 0; // paigutatava lipu indeks
			int at = 0;

			// permutatsioonide hulk
			listlist = new int[3628800][list.size()];

			do {
				if (laud[n] < suurus) {
					laud[n]++;
					if (onTules(n))
						continue;
					if (n < suurus - 1) {
						n++;
						continue;
					} else {
						// lahend käes!!!
						for (int i = 0; i < list.size(); i++) { // laud.length
							listlist[at][i] = laud[i] - 1;
							// System.out.print(listlist[at][i]);
						}
						// System.out.println();
						at = at + 1;
						// tootle();
					}
				} else {
					laud[n] = 0; // lipp laualt ära
					n--;
				}
			} while (n >= 0);
			for (int i = 0; i < listlist.length; i++) {
				for (int j = 0; j < listlist[i].length; j++) {
					// System.out.print(listlist[i][j] + " ");
				}
				// System.out.println();
			}
			System.out.println("Kõik permutatsioonid leitud!"); // REMOVE!!!
		}

		/**
		 * lahendi töötlemine
		 * 
		 * @author jpoial
		 */
		public void tootle() {
			System.out.print("meetod tootle() ");
			System.out.println(toString());
		}

		/**
		 * sõneksteisendus
		 * 
		 * @author jpoial
		 * */
		public String toString() {
			StringBuilder sb = new StringBuilder();
			if (suurus < 2)
				return "1 ";
			for (int i = 0; i < suurus; i++) {
				sb.append(String.valueOf(laud[i]) + " ");
			}
			return sb.toString();
		}

	} // Lipud l6pp

}
