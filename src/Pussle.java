import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

/*
 *  brute force random
 *  Lauri bitbucket https://bitbucket.org/llaidna/ajas-kodu07
 * 
 *  Mõistatuste lahendaminea
 *  Koostada programm, mis lahendaks kahe arvu liitmise kohta käivaid
 *  sõnamõistatusi, milles igale tähele tuleb vastavusse seada erinev
 *  kümnendnumber ning ühegi arvu alguses ei saa olla null (sõnad
 *  koosnevad ladina tähestiku suurtähtedest ning ühegi sõna pikkus ei
 *  ületa 18 tähte).
 *  
 *  Näiteks mõistatuse SEND + MORE = MONEY lahendiks on
 *  S=9, E=5, N=6, D=7, M=1, O=0, R=8, Y=2 (9567 + 1085 = 10652)
 *  Sõnad saadakse käsurea parameetritena (kolm sõna). Programm peaks
 *  mittelahenduva mõistatuse korral seda ütlema ning lahenduvale
 *  mõistatusele leidma vähemalt ühe lahendi. Väljundi võite vabalt
 *  kujundada, samuti kasutatavad andmestruktuurid.
 *  
 *  Lahendus esitage ikka https://moodle.e-ope.ee/course/view.php?id=2454
 *  Õigeaegne ja piisav lahendus annab 10 punkti.
 *  
 *  http://www.tutorialspoint.com/java/java_hashmap_class.htm
 *  
 *  jupitame sissetulevad sõnad
 *  kui kolmandas sõnas on rohkem char kui esimeses või teises sõnas, on kolmanda esimene char 1 (foundOne = true)
 *  
 *  võtame kõik tähed, paneme neile numbrid 0...n
 *  siis hakkame viimasele proovima +1 suuremaid väärtusi 10x
 *  kui ei klapi
 *  siis suurendama eelviimast +1 ja proovime ikkagi viimasele väärtusi (neid mis pole eespool käigus hetkel)
 *  kui ei klapi
 *  siis suurendame eelviimast +1 ja proovime ikkagi viimasele väärtusi (neid mis pole eespool käigus hetkel)
 *  
 *  HashMap itereerimise kood kopeeritud veebist!!!
 *  Timeout kood kopteeritud veebist!!! https://stackoverflow.com/questions/2275443/how-to-timeout-a-thread
 */

//public class Pussle {

	static String[] myArgs = new String[3];
	static Map<String, Integer> hm = new LinkedHashMap<String, Integer>();
	static LinkedList<LinkedList> tried = new LinkedList<LinkedList>(); // list, kus sees on all tried väärtused

	private static Random random; // random

	public static void main(String[] args) {
		random = new Random();
		
		for (int i = 0; i < args.length; i++) {
			myArgs[i] = args[i];
			System.out.println("sisse " + myArgs[i]);
		}

		String erandone = Character.toString(myArgs[0].charAt(0)); // sõna 1 esitäht mis ei tohi olla 0
		String erandtwo = Character.toString(myArgs[1].charAt(0)); // sõna 2 esitäht mis ei tohi olla 0
		String erandthree = Character.toString(myArgs[2].charAt(0)); // sõna 3 esitäht mis ei tohi olla 0

		System.out.println("erandid ehk algustähed mis ei tohi 0 olla: " + erandone + " & " + erandtwo + " & " + erandthree);

		LinkedList<String> list = new LinkedList<String>();  // list, kus sees on käigus olevad tähed
		LinkedList<Integer> values = new LinkedList<Integer>(); // list, kus sees on hetkel väärtustatud väärtused
		
		for (int i = 0; i < 3; i++) { // iterates number of words
				for (int j = 0; j < myArgs[i].length(); j++) { // iterates number of chars
					hm.put(Character.toString(myArgs[i].charAt(j)), null); // puts chars into hm
				}
		}

//		hm.replace("S", 9); hm.replace("E", 5);	hm.replace("N", 6);	hm.replace("D", 7);	hm.replace("M", 1);	hm.replace("O", 0);	hm.replace("R", 8);	hm.replace("Y", 2);
//		hm.replace("A", ); hm.replace("E", 5);	hm.replace("N", 6);	hm.replace("D", 7);	hm.replace("M", 1);	hm.replace("O", 0);	hm.replace("R", 8);	hm.replace("Y", 2);

		// teeb ühekordse listi tähtedest - kood veebist!!!
		// Get a set of the entries
		Set<Entry<String, Integer>> set = hm.entrySet();
		// Get an iterator
		Iterator<Entry<String, Integer>> it = set.iterator();
		// Display elements
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry me = (Map.Entry) it.next();
			System.out.print(me.getKey() + "=");
			System.out.print(me.getValue());
			System.out.print(" ");
			list.add((String) me.getKey());
		}
		System.out.println();
		System.out.println("list = " + list);

		
		// paneme väärtused hm'i
		for (int i = 0; i < list.size(); i++) { // nii mitu tähte on kokku käigus
			hm.replace(list.get(i), i); // 1...9 elementidele. Võtab listist elemendi, näiteks "A" ja paneb hm'is sellele väärtuse
			values.add(i); // adds values to values list, which holds all the values currently in the list
		}
		System.out.println("hm is now: " + hm);

		// põhi kood!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		while (arvuta() == false) {
			
			int value = random.nextInt(((9 - 0) + 1) + 0); // random number
			int letter = random.nextInt(list.size()); // random täht
			int letter2 = random.nextInt(list.size()); // random täht

			if ((letter == 0 && value == 0) || ((list.get(letter)).equals(erandtwo) == true && value == 0) || ((list.get(letter).equals(erandthree) == true && value == 0))) {
				value = 1;
			}

			if (values.size() <= 9 && values.contains(value) == false) { // käitumine alla 10 tähe puhul, tee ainult kui juba sellist arvu käigus pole
				values.remove(hm.get(list.get(letter))); // eemaldab listist int
				values.add(value); // lisab listi uue int
				hm.replace(list.get(letter), value);
			}
			
			if (values.size() == 10) { // käitumine 10 erineva tähe puhul
				
//				values.remove(hm.get(list.get(letter))); // eemaldab listist int
//				values.add(value); // lisab listi uue int
				
//				if (tried.contains(values) == false) {
//					tried.add(values);
//					System.out.println(values);
//				}
														// value - random number 0...9
														// letter - random täht
				int ajutint = hm.get(list.get(letter)); // võtab random letter alt väärtuse
				hm.replace(list.get(letter), hm.get(list.get(letter2))); // paneb selle random letter alla uue random väärtuse
				hm.replace(list.get(letter2), ajutint); // paneb algselt võetud väärtuse selle asemele, kuhu uus väärtus läks
			}
			
		}

	}
	
//			while (values.contains(abi) == true) {
//				abi = random.nextInt(10);
//				System.out.println("abi in while = " + abi);
//			}
//			for (int i = 0; i < list.size(); i++) {
//				if (values.contains(abi) == false) {
//					hm.replace(list.get(i), abi);
//					values.remove(hm.get(list.get(i)));
//					values.add(abi);
//					System.out.println("if... abi = " + abi);
//				} else {
//					System.out.println("else... abi = " + abi);
//				}
//
//			}
//			System.out.println("esimene abi = " + abi + " ja does contain? " + values.contains(abi));
//			}			
//			if (values.contains(counter) == false) { // kui values'is pole counter valuet, siis lisa see 
//				hm.replace(list.get(counter), hm.get(list.get(counter)) + 1); // suurendab hm'is counter-kohal oleva tähe väärtust +1 võrra
//			} else {
//				counter = counter + 1;
//			}

//		System.out.println("hm peale väärtustamist =   " + hm);
//		System.out.println("arvuta = " + arvuta());
		
		//		System.out.println();
//		// Deposit 1000 into Zara's account
//		int balance = hm.get("E");
//		hm.put("E", (balance + 1));
//		System.out.println("E new: " + hm.get("E"));
		
	/**
	 * Arvutab
	 * 
	 * @return boolean
	 */
	public static boolean arvuta() {
		StringBuilder sbone = new StringBuilder();
		StringBuilder sbtwo = new StringBuilder();
		StringBuilder sbthree = new StringBuilder();
		long one = 0;
		long two = 0;
		long three = 0;
		long sum = 0;

		for (int i = 0; i < myArgs[0].length(); i++) {
//			stringone = stringone + hm.get(Character.toString(myArgs[0].charAt(i)));
			sbone.append(hm.get(Character.toString(myArgs[0].charAt(i))));
		}
		for (int i = 0; i < myArgs[1].length(); i++) {
//			stringtwo = stringtwo + hm.get(Character.toString(myArgs[1].charAt(i)));
			sbtwo.append(hm.get(Character.toString(myArgs[1].charAt(i))));
		}
		for (int i = 0; i < myArgs[2].length(); i++) {
//			stringthree = stringthree + hm.get(Character.toString(myArgs[2].charAt(i)));
			sbthree.append(hm.get(Character.toString(myArgs[2].charAt(i))));
		}
		
//		sbone.append(myArgs[0]);
//		System.out.println(sbone.toString());
		one = Long.parseLong(sbone.toString()); 
		two = Long.parseLong(sbtwo.toString());
		three = Long.parseLong(sbthree.toString());
		sum = one + two;

		if (one + two == three) {
			System.out.println("   " + one);
			System.out.println("   " + two);
			System.out.println("  " + sum);
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! DONE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.out.println("Summa on: " + sum + " ja peakski olema " + three);
			System.out.println("Õiged väärtused on =   " + hm);
			return true;
		}

//		 System.out.println("Summa on: " + sum + " aga peaks olema " + three + " hm = " + hm);
		return false;
	}
}
