//import java.util.ArrayList;
//
//
//public class Lipud {
//
//	/** laua mõõde */
//	int suurus;
//
//	/** jooksev seis, 0 <= laud [i] <= suurus */
//	int[] laud;
//	
////	ArrayList<ArrayList<Integer>> listlist = new ArrayList<ArrayList<Integer>>();
//
//	/** laua algseis */
//	Lipud(int n) {
//		if (n < 1)
//			throw new RuntimeException("valed mõõtmed");
//		suurus = n;
//		laud = new int[suurus];
//		for (int i = 0; i < suurus; i++) {
//			laud[i] = 0;
//		}
//	}
//
//	public static void main(String[] p) {
//		System.out.println("Lippude paigutamine");
//		Lipud seis = new Lipud(10); // laua suurus
//		seis.paiguta();
//	} // main() l6pp
//
//	/** laua seisu analüüs lipu nr. k jaoks: 1 <= k <= suurus-1 ) */
//	public boolean onTules(int k) {
//		if (k >= suurus)
//			throw new RuntimeException("Vale lipu number");
//		if (k < 1)
//			return false;
//		for (int i = 0; i < k; i++) {
//			if (laud[i] == laud[k])
//				return true; // samal real
//			if (k - i == Math.abs(laud[k] - laud[i]))
//				return true; // samal diagonaalil
//		}
//		return false;
//	}
//
//	/** paigutamine */
//	public void paiguta() {
//		int n = 0; // paigutatava lipu indeks
//		do {
//			if (laud[n] < suurus) {
//				laud[n]++;
//				if (onTules(n))
//					continue;
//				if (n < suurus - 1) {
//					n++;
//					continue;
//				} else {
//					// lahend käes!!!
//					tootle();
//				}
//			} else {
//				laud[n] = 0; // lipp laualt ära
//				n--;
//			}
//		} while (n >= 0);
//		System.out.println("Kõik lahendid leitud sdf"); // REMOVE!!!
//	}
//
//	/** lahendi töötlemine */
//	public void tootle() {
//		System.out.print("meetod tootle() ");
//		System.out.println(toString());
//	}
//
//	/** sõneksteisendus */
//	public String toString() {
//		StringBuffer sb = new StringBuffer();
//		if (suurus < 2)
//			return "1 ";
//		for (int i = 0; i < suurus; i++) {
//			sb.append(String.valueOf(laud[i]) + " ");
//		}
//		return sb.toString();
//	}
//
//} // Lipud l6pp
